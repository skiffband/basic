<?php


return [
    'request' => [
        'class' => skiff\components\request\Request::class,

    ],
    'router' => [
        'class' => \skiff\components\router\Router::class
    ]
];