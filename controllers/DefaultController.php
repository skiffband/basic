<?php

namespace controllers;

use skiff\base\controller\Controller;

class DefaultController extends Controller
{
    public function index()
    {
        echo $this->view('index');
    }
}