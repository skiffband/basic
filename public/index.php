<?php

define('DEBUG_MODE', true);

require dirname(__DIR__).'/vendor/autoload.php';

require dirname(__DIR__).'/vendor/skiff-band/skiff-framework/App.php';

$config = require dirname(__DIR__).'/config/app.php';

App::get()->init($config);